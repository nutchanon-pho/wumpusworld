package com.sample;

public class Scream implements Predicate, Perception {
	private int x;
	private int y;

	public Scream(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

}
