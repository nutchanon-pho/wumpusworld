package com.sample;

public class Glitter  implements Predicate, Perception{
	private int x;
	private int y;

	public Glitter(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public String toString(){
		return "*";
	}

}
