package com.sample;

public interface Predicate {
	public int getX();
	public int getY();
}
