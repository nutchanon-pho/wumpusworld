package com.sample;

public class Agent implements Predicate {
	private int x;
	private int y;
	private int direction;
	private boolean isAlive;
	private boolean grabGold;
	private boolean hasShot;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void grabGold() {
		grabGold = true;
	}

	public boolean isGrabGold() {
		return grabGold;
	}

	public void setGrabGold(boolean grabGold) {
		this.grabGold = grabGold;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public void die() {
		isAlive = false;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public static final int NORTH = 0, EAST = 1, SOUTH = 2, WEST = 3;

	public Agent(int x, int y, int direction) {
		this.x = x;
		this.y = y;
		this.direction = direction;
		this.grabGold = false;
		isAlive = true;
		hasShot = false;
	}

	public String toString() {
		String str = "";
		switch (direction) {
		case Agent.NORTH:
			str = "^";
			break;
		case Agent.EAST:
			str = ">";
			break;
		case Agent.WEST:
			str = "<";
			break;
		case Agent.SOUTH:
			str = "v";
			break;
		}
		return str;
	}

	public void generateMove(int destX, int destY) {
		int targetDirection = -1;
		if (destX > x) {
			targetDirection = Agent.SOUTH;
		} else if (destX < x) {
			targetDirection = Agent.NORTH;
		} else if (destY > y) {
			targetDirection = Agent.EAST;
		} else if (destY < y) {
			targetDirection = Agent.WEST;
		}

		int currentDirection = direction;
		int turnRightCount = 0;
		while (currentDirection != targetDirection) {
			currentDirection = (currentDirection + 1) % 4;
			turnRightCount++;
		}

		currentDirection = direction;
		int turnLeftCount = 0;
		while (currentDirection != targetDirection) {
			currentDirection--;
			if (currentDirection < 0)
				currentDirection = 3;
			turnLeftCount++;
		}

		int turnCount = 0;
		String turnAction = "";
		if (turnLeftCount > turnRightCount) {
			turnCount = turnRightCount;
			turnAction = "Turn Right";
		} else {
			turnCount = turnLeftCount;
			turnAction = "Turn Left";
		}
		// set new direction
		direction = targetDirection;
		if (turnCount != 0)
			System.out.println(turnAction + " " + turnCount + "time(s)");
	}

	public boolean HasShot() {
		return hasShot;
	}

	public void setHasShot(boolean hasShot) {
		this.hasShot = hasShot;
	}

	public void moveTo(int x, int y) {
		this.x = x;
		this.y = y;
		System.out.println("Move Forward");
	}
}
