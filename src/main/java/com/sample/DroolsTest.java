package com.sample;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

/**
 * This is a sample class to launch a rule.
 */
public class DroolsTest {

	public static final void main(String[] args) {
		// Initialise every predicate
		Agent agent = new Agent(3, 0, Agent.EAST);
		World board = new World();

		// Initialise world
		World world = new World();
		generateWorld(world);
		System.out.println(world);
		ArrayList<Safe> visited = new ArrayList<Safe>();
		try {
			// load up the knowledge base
			KieServices ks = KieServices.Factory.get();
			KieContainer kContainer = ks.getKieClasspathContainer();
			KieSession kSession = kContainer.newKieSession("ksession-rules");

			kSession.insert(board);
			FactHandle agentFact = kSession.insert(agent);
			// Start
			int round = 0;
			agent.setX(3);
			agent.setY(0);
			while (true) {

				ArrayList<Predicate> perception = world.getCell(agent.getX(),
						agent.getY()).predicateList;

				// Insert perception to knowledge base
				for (Predicate p : perception) {
					if (p instanceof Pit) {
						agent.die();

						break;
					}
					if (p instanceof Wumpus && world.isWumpusAlive()) {
						agent.die();
					}
					if (p instanceof Perception)
						kSession.insert(p);
				}
				if (!agent.isAlive()) {
					break;
				} else {
					Safe s = new Safe(agent.getX(), agent.getY());
					kSession.insert(s); // not die insert Safe
					visited.add(s);
				}
				kSession.update(agentFact, agent);
				kSession.fireAllRules();

				// Update board
				@SuppressWarnings("unchecked")
				Collection<Object> results = (Collection<Object>) kSession
						.getObjects();
				World displayBoard = new World();

				ArrayList<Safe> safeList = new ArrayList<Safe>();
				ArrayList<Pit> pitList = new ArrayList<Pit>();
				ArrayList<Wumpus> wumpusList = new ArrayList<Wumpus>();
				for (Object c : results) {

					if (c instanceof Predicate) {
						Predicate p = (Predicate) c;
						displayBoard.addPredicate(p, false);
						if (c instanceof Safe) {
							Safe s = (Safe) c;
							if (!visited.contains(s)) {
								safeList.add(s);
							}
						}
						if (c instanceof Pit) {
							Pit pit = (Pit) c;
							if (!visited.contains(pit)) {
								pitList.add(pit);
							}
						}
						if (c instanceof Wumpus) {
							Wumpus wumpus = (Wumpus) c;
							if (!visited.contains(wumpus)) {
								wumpusList.add(wumpus);
							}
						}
					}
				}
				// if (round == 0) {
				System.out.println(displayBoard);
				// }

				// update move
				if (agent.isGrabGold()) {
					ArrayList<Safe> moveList = new ArrayList<Safe>();
					findPath(visited, new Safe(agent.getX(), agent.getY()),
							new Safe(3, 0), moveList);
					for (Safe s : moveList) {
						agent.generateMove(s.getX(), s.getY());
						agent.moveTo(s.getX(), s.getY());
						results = (Collection<Object>) kSession.getObjects();
						displayBoard(results);
					}
					break;
				} else if (!safeList.isEmpty()) {
					Safe destination = safeList.get(0);

					ArrayList<Safe> moveList = new ArrayList<Safe>();
					findPath(visited, new Safe(agent.getX(), agent.getY()),
							destination, moveList);
					for (Safe s : moveList) {
						agent.generateMove(s.getX(), s.getY());
						agent.moveTo(s.getX(), s.getY());
						results = (Collection<Object>) kSession.getObjects();
						displayBoard(results);
					}
				} else if (!agent.HasShot() && !wumpusList.isEmpty()) {
					int randomIndex = (int) Math.random() * pitList.size();
					Wumpus w = wumpusList.get(randomIndex);
					Safe goTo = new Safe(agent.getX(), agent.getY());
					if (!goTo.isAdjacent(w)) {
						for (Safe s : visited) {
							if (s.isAdjacent(w)) {
								goTo = s;
								break;
							}
						}

						ArrayList<Safe> moveList = new ArrayList<Safe>();
						findPath(visited, new Safe(agent.getX(), agent.getY()),
								goTo, moveList);

						for (Safe s : moveList) {
							agent.generateMove(s.getX(), s.getY());
							agent.moveTo(s.getX(), s.getY());
							results = (Collection<Object>) kSession
									.getObjects();
							displayBoard(results);
						}
					}
					agent.generateMove(w.getX(), w.getY());
					Predicate result = world.fireWumpus(w.getX(), w.getY());
					System.out.println("Fire Arrow");
					if (result instanceof Scream)
						System.out.println("Scream");
					agent.setHasShot(true);
					kSession.insert(result);
				} else {
					int randomIndex = (int) Math.random() * pitList.size();
					Pit destination = pitList.get(randomIndex);
					Safe goTo = new Safe(destination.getX(), destination.getY());
					ArrayList<Safe> moveList = new ArrayList<Safe>();
					findPath(visited, new Safe(agent.getX(), agent.getY()),
							goTo, moveList);
					for (Safe s : moveList) {
						agent.generateMove(s.getX(), s.getY());
						agent.moveTo(s.getX(), s.getY());
						results = (Collection<Object>) kSession.getObjects();
						displayBoard(results);
					}
				}
				round++;
			}
			if (!agent.isAlive()) {
				System.out.println("Agent died.@" + agent.getX() + ","
						+ agent.getY());
			} else if (agent.isGrabGold()) {
				System.out.println("WINNNN");
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public static void findPath(ArrayList<Safe> visited, Safe current,
			Safe destination, ArrayList<Safe> moves) {
		if (destination == null)
			return;
		if (destination.isAdjacent(current)) {
			moves.add(0, destination);
		} else {
			Safe nextDestination = null;
			for (Safe s : visited) {
				if (destination.isAdjacent(s) && !moves.contains(s)) {
					nextDestination = s;
					break;
				}
			}
			moves.add(0, destination);
			findPath(visited, current, nextDestination, moves);
		}
	}

	public static void generateWorld(World world) {
		int x = (int) (Math.random() * 4);
		int y = (int) (Math.random() * 4);
		
		while (x == 3 && y == 0) {
			x = (int) (Math.random() * 4);
			y = (int) (Math.random() * 4);
		}
		world.addPredicate(new Wumpus(x, y), true);
		for (int i = 0; i < 3; i++) {
			x = (int) (Math.random() * 4);
			y = (int) (Math.random() * 4);
			while (x == 3 && y == 0) {
				x = (int) (Math.random() * 4);
				y = (int) (Math.random() * 4);
			}
			world.addPredicate(new Pit(x, y), true);
		}
		x = (int) (Math.random() * 4);
		y = (int) (Math.random() * 4);
		world.addPredicate(new Glitter(x, y), true);
	}

	public static void displayBoard(Collection<Object> results) {
		// Display Board
		World displayBoard = new World();
		for (Object c : results) {
			if (c instanceof Predicate) {
				Predicate p = (Predicate) c;
				displayBoard.addPredicate(p, false);
			}
		}
		System.out.println(displayBoard);
	}
}
