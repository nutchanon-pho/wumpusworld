package com.sample;

public class World {
	Cell[][] world;
	private boolean wumpusAlive;
	public World() {
		world = new Cell[4][4];
		for (int i = 0; i < world.length; i++) {
			for (int j = 0; j < world[0].length; j++) {
				world[i][j] = new Cell();
			}
		}
		wumpusAlive=true;
	}

	public boolean isValid(int x, int y) {
		if (x < 0 || x >= world.length || y < 0 || y >= world.length)
			return false;
		else
			return true;

	}

	public void addPredicate(Predicate p, boolean refTable) {
		int x = p.getX();
		int y = p.getY();
		world[x][y].addPredicate(p);
		if (refTable) {// ReferenceTable
			if (p instanceof Wumpus) {
				if (isValid(x - 1, y) && !world[x - 1][y].containsStench()) {
					world[x - 1][y].addPredicate(new Stench(x - 1, y));
				}
				if (isValid(x, y + 1) && !world[x][y + 1].containsStench()) {
					world[x][y + 1].addPredicate(new Stench(x, y + 1));
				}
				if (isValid(x + 1, y) && !world[x + 1][y].containsStench()) {
					world[x + 1][y].addPredicate(new Stench(x + 1, y));
				}
				if (isValid(x, y - 1) && !world[x][y - 1].containsStench()) {
					world[x][y - 1].addPredicate(new Stench(x, y - 1));
				}
			} else if (p instanceof Pit) {
				if (isValid(x - 1, y) && !world[x - 1][y].containsBreeze()) {
					world[x - 1][y].addPredicate(new Breeze(x - 1, y));
				}
				if (isValid(x, y + 1) && !world[x][y + 1].containsBreeze()) {
					world[x][y + 1].addPredicate(new Breeze(x, y + 1));
				}
				if (isValid(x + 1, y) && !world[x + 1][y].containsBreeze()) {
					world[x + 1][y].addPredicate(new Breeze(x + 1, y));
				}
				if (isValid(x, y - 1) && !world[x][y - 1].containsBreeze()) {
					world[x][y - 1].addPredicate(new Breeze(x, y - 1));
				}
			}
		}
	}

	public Cell getCell(int x, int y) {
		return world[x][y];
	}

	public boolean isWumpusAlive() {
		return wumpusAlive;
	}

	public Predicate fireWumpus(int x, int y) {
		if (world[x][y].containsWumpus()) {
			wumpusAlive = false;
			return new Scream(x, y);
		} else {
			return new NoWumpus(x, y);
		}
	}

	public String toString() {
		String str = "";
		for (int i = 0; i < world.length; i++) {
			for (int j = 0; j < world.length; j++) {
				str += world[i][j] + "  ";
			}
			str += "\n";
		}
		return str;
	}
}
