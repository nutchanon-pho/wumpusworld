package com.sample;

public class Safe implements Predicate {
	private int x;
	private int y;

	public Safe(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public String toString(){
		return "S";
	}
	public boolean equals(Object other){
		
		Predicate another = (Predicate) other;
		if(another.getX() == x && another.getY() == y)
			return true;
		else
			return false;
	}
	
	public  boolean isAdjacent(Predicate s ){
		if(x+1 == s.getX() && y == s.getY()) return true;
		if(x-1 == s.getX() && y == s.getY()) return true;
		if(x == s.getX() && y+1 == s.getY()) return true;
		if(x == s.getX() && y-1 == s.getY()) return true;
		else return false;
	}
	
}
