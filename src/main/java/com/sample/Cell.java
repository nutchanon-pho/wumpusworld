package com.sample;

import java.util.ArrayList;

public class Cell {
	ArrayList<Predicate> predicateList;

	public Cell() {
		predicateList = new ArrayList<Predicate>();
	}

	public void addPredicate(Predicate p) {
		if (p instanceof Safe && containsSafe())
			return;
		if (p instanceof Breeze && containsBreeze())
			return;
		if (p instanceof Stench && containsStench())
			return;
		if (p instanceof Wumpus && containsWumpus())
			return;
		if (p instanceof Pit && containsPit())
			return;
		if (p instanceof Glitter && containsGlitter())
			return;
		predicateList.add(p);
	}

	public String toString() {
		String str = "[";
		int count = 10;
		for (Predicate p : predicateList) {
			if (p instanceof Safe || p instanceof Wumpus || p instanceof Pit
					|| p instanceof Breeze || p instanceof Stench
					|| p instanceof Glitter || p instanceof Agent) {
				str += p.toString() + " ";
				count=count-2;
			}
		}
		for(int i=0 ;i<count ;i++){
			str+=" ";
		}
		str += "]";
		return str;
	}

	public boolean containsBreeze() {
		for (Predicate p : predicateList) {
			if (p instanceof Breeze)
				return true;
		}
		return false;
	}

	public boolean containsStench() {
		for (Predicate p : predicateList) {
			if (p instanceof Stench)
				return true;
		}
		return false;
	}

	public boolean containsSafe() {
		for (Predicate p : predicateList) {
			if (p instanceof Safe)
				return true;
		}
		return false;
	}

	public boolean containsPit() {
		for (Predicate p : predicateList) {
			if (p instanceof Pit)
				return true;
		}
		return false;
	}

	public boolean containsWumpus() {
		for (Predicate p : predicateList) {
			if (p instanceof Wumpus)
				return true;
		}
		return false;
	}

	public boolean containsGlitter() {
		for (Predicate p : predicateList) {
			if (p instanceof Glitter)
				return true;
		}
		return false;
	}
}
