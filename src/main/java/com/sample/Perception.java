package com.sample;

public interface Perception {
	public int getX();
	public int getY();
}
