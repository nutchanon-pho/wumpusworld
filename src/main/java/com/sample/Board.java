package com.sample;

public class Board {
	int[][] board;
	public static final int BLANK = 0, WUMPUS = 1, PIT = 2, STENCH = 3,
			BREEZE = 4, GLITTER = 5, AGENT = 6, SAFE = 7;
	Agent agent;
	boolean isFinished;
	

	public Board() {
		board = new int[4][4];
		isFinished = false;
	}

	public Board(Agent agent) {
		board = new int[4][4];
		setAgent(agent);
		isFinished = false;
	}

	public void setPredicate(Predicate p) {
		if (p instanceof Agent) {
			board[p.getX()][p.getY()] = Board.AGENT;
		} else if (p instanceof Wumpus) {
			board[p.getX()][p.getY()] = Board.WUMPUS;
		} else if (p instanceof Pit) {
			board[p.getX()][p.getY()] = Board.PIT;
		} else if (p instanceof Stench) {
			board[p.getX()][p.getY()] = Board.STENCH;
		} else if (p instanceof Breeze) {
			board[p.getX()][p.getY()] = Board.BREEZE;
		} else if (p instanceof Glitter) {
			board[p.getX()][p.getY()] = Board.GLITTER;
		} else if (p instanceof Safe) {
			board[p.getX()][p.getY()] = Board.SAFE;
		}
	}

	public void setAgent(Agent a) {
		this.agent = a;
		board[agent.getX()][agent.getY()] = Board.AGENT;
	}

	public void setWumpus(Wumpus w) {
		board[w.getX()][w.getY()] = Board.WUMPUS;
	}

	public void setPit(Pit p) {
		board[p.getX()][p.getY()] = Board.PIT;
	}

	public void setStench(Stench s) {
		board[s.getX()][s.getY()] = Board.STENCH;
	}

	public void setBreeze(Breeze b) {
		board[b.getX()][b.getY()] = Board.BREEZE;
	}

	public void setGlitter(Glitter g) {
		board[g.getX()][g.getY()] = Board.GLITTER;
	}

	public void setSafe(Safe s) {
		board[s.getX()][s.getY()] = Board.SAFE;
	}

	public String toString() {
		setAgent(agent);
		String str = "";
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (board[i][j] == Board.AGENT) {
					int agentDirection = agent.getDirection();
					char direction = '\0';
					switch (agentDirection) {
					case Agent.NORTH:
						direction = '^';
						break;
					case Agent.EAST:
						direction = '>';
						break;
					case Agent.WEST:
						direction = '<';
						break;
					case Agent.SOUTH:
						direction = 'v';
						break;
					}
					str += "[" + direction + "]      ";
				} else
					str += "[" + board[i][j] + "]      ";
			}
			str += "\n";
		}
		return str;
	}

	public boolean isValid(int x, int y) {
		if (x < 0 || x >= board.length || y < 0 || y >= board.length)
			return false;
		else
			return true;

	}

}
